FROM olproject/olang

RUN mkdir /playground
COPY . /playground/
WORKDIR /playground
RUN olang migrate.ola

CMD ["olang","boot.ola"]
EXPOSE 9009
