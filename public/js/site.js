// new class
class OLanguagePlayground {
    constructor(selector) {
        this.el = $(selector);
        this.executePath = "/execute"
        this.getResult = "/result"
    }

    ajax(url, data, callback) {
        return $.ajax({
            url: url,
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'X-App-Framework': 'O Language Framework'
            },
            data: data,
            success: callback
        });
    }

    editor() {
        this.editorEl = CodeMirror.fromTextArea(document.querySelector(".code-editor"), {
            lineNumbers: true,
            theme: "dracula",
          });
        var code = document.querySelector(".code-editor").innerHTML;
        if (code != '') {
            this.editorEl.setValue(code);
        }else{
            var code = 'def hello = fn() {\n\treturn "Hello World!"\n}\nhello()';
            this.editorEl.setValue(code);
        }
        return this.editorEl;
    }

    setShareLink(link) {
        this.el.find(".share-modal").find(".share-link").val(link);
        if(this.getShareLink() == ""){
            this.el.find('.share').hide();
        }else{
            this.el.find('.share').show();
        }
        
    }

    getShareLink() {
        return this.el.find(".share-modal").find(".share-link").val()
    }


    execute(code) {
        this.ajax(this.executePath, {code: code}, (response) => {
            this.setShareLink(window.location.origin+"/code/"+response.id);
            if(typeof response.result == "string"){
                response = response.result.replace(/[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g, '');
            }
            else{
                response = response.result;
            }

            this.response = response;
            
            this.el.find('.output-code').html(response);
        });
    }


    init() {
        this.editor();
        this.el.find('.run-code').on('click', (e) => {
            var code = this.editorEl.getValue();
            this.execute(code);
        });
        
        console.log(this.getShareLink());

        if(this.getShareLink() == ""){
            this.el.find('.share').hide();
        }else{
            this.el.find('.share').show();
        }
            

        var shareModal = this.el.find('.share-modal');

        this.el.find('.share').on('click', (e) => {
            shareModal.modal('show');
        });

        this.el.find(".share-close").on('click', function() {
            shareModal.modal("hide");
        });


        return this;
    }
}